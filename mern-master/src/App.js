import React from 'react';

/* import Content  from './components/content.js'; */

import {Header} from './components/header'
import {Provider} from 'react-redux';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Login from './components/login';
/* import Signup from './components/signup'; */ 
import Cities from './components/cities' 
import Links from './components/alinks'
import Itineraris from './components/itineraris'


import store from './store';

import './App.css';
import { Footer } from './components/footer';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
     
      <Router>
        <Header/>
                <Switch>
                <Route exact path="/Login" component={Login} />    
               {/*   <Route  path="/" component={Content}/> */}
                   <Route exact={true} path="/" component={Links} />
                  <Route exact path="/cities" component={Cities} />
                  {<Route path="/itineraries/:city" component={Itineraris} />}
                 {/*  <Route path="/login" component={Login} /> */}
                 {/*  <Route path="/signup" component={Signup} /> */}
                </Switch>  
     <Footer/>
      </Router>
    </div>
    </Provider>
  );
}

export default App;
