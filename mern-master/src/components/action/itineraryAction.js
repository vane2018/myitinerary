import { FETCH_ITINERARIES, FETCH_ALL_ITINERARIES } from "./type";
import axios from "axios";

export const fetchItineraries = city=> dispatch => {
  console.log("Axios working on Itineraries!");
  axios
/*     .get(`http://localhost:3001/api/${city}`) */
    .get(`http://localhost:3001/api/itineraries/${city}`)
    .then(res => {
      console.log(res);
      dispatch({
        type: FETCH_ITINERARIES,
        payload: res.data
        
      });
    })
    .catch(err => {
      console.log(err);
    });
   
};

export const fetchAllAxiosItineraries = () => dispatch => {
  axios.get("http://localhost:3001/api/itineraries").then(res => {
    dispatch({
      type: FETCH_ALL_ITINERARIES,
      payload: res.data
    });
  });
};