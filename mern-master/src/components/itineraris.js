import React,{Component} from 'react';
/* import {getItineraris} from './action/citiesAction'; */
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import reply from "../components/imagenes/reply.png";
import error from "../components/imagenes/404-error.jpg";
import {Card} from 'reactstrap';
import { fetchItineraries } from "../components/action/itineraryAction";
import Activities from "../components/Activities";
import Toogle from "../components/Toogle";


import { NavLink } from "react-router-dom";

class Itineraris extends React.Component {
 

  constructor(props){
    super(props)
    this.state={
      city:""
    }
  }


  async fetchItineraries() {
  /*   console.log(this.props.history.location.pathname)
    var city = this.props.history.location.pathname; */
    var city = this.props.match.params.city;
    console.log(this.props.match);
    console.log(city);
    this.setState({ city: city });
    console.log(city);
    await this.props.fetchItineraries(city);
}

componentDidMount() {
  console.log("entro a itinerario")
    this.fetchItineraries();

}

   
 /*  static propTypes = {
    getItineraris: PropTypes.func.isRequired,
    itinerary: PropTypes.object.isRequired,  */
  // isAuthenticated: PropTypes.bool
/* }; */
  /*   state ={
        listaPaises: []
    } */
 /*    static propType={
      getCities:this.propType.func.isRequired,
      city:PropTypes.object.isRequired
    }; */
  
   /*   async componentDidMount(){ */
     /*  const res =  await axios.get('http://localhost:5000/api/city');
      this.setState({listaPaises:res.data.cities});
      console.log(this.state.listaPaises); */
    /*   await this.props.getItineraris();
    } */


    render() {
    /*   const itinerary = this.props.itineraris;
      console.log(itinerary); */
     /*  console.log(JSON.stringify(cities)) */
      return(
        <React.Fragment>
         
        <main>
            <div className="headerItineraries">
                <NavLink to="/Cities">
                    <img src={reply} alt="backToCities" className="backCities" />
                </NavLink>
                <h1 className="ItinerariesTitle">{this.state.cities}</h1>
            </div>
            {this.props.itineraris.length === 0 ?
                <div>
                    <h1 className="NoItineraries"> Oops seems that there is no Itinerary for this city yet! </h1>
                    <img src={error} alt="404" className="error" />
                </div>
                : this.props.itineraris.map(itineraries => (
                    <React.Fragment key={itineraries._id}>
                        <Card className=" ItinerariesCard">
                            <div className="flexItineraries">
                                <div className="Profile-box">
                                    <img src={itineraries.profileImg} alt="profile-img" className="profileImg" />
                                    <p className="profileName">{itineraries.profileName}</p>
                                </div>

                                <div className="Itineraries-box">
                                    <div>
                                        <p className="ItTit">{itineraries.title}</p>
                                    </div>
                                    <div className="flexItineraries">
                                        <p className="Rating"> Likes: {itineraries.likes}</p>
                                        <p className="Rating">{itineraries.hours} Hours</p>
                                        <p className="Rating">{itineraries.price}</p>
                                    </div>
                                    <p className="Rating">{itineraries.hashtags}</p>
                                </div>
                               {/*  <div>
                                    {localStorage.usertoken ? 
                                        <Favourite ItineraryId = {itineraries._id} UserId = {this.state.UserId}/>
                                        : ""
                                    }
                                </div> */}

                                <div className="Activities">
                             <Toogle>
                                <Activities activities={itineraries.activities}/>
                            </Toogle>
                            </div>
                            </div>

                         
                    
                        </Card>
                    </React.Fragment>
                ))}
        </main>
        <footer className="homeFooter flex">
            <div className="contenedor home">
              {/*   <Home /> */}
            </div>
        </footer>
    </React.Fragment>
   


  
      )
 
   
        
    }
};

Itineraris.propTypes = {
  fetchItineraries: PropTypes.func.isRequired,
  itinerary: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    
  itineraris: state.itinerary.itineraris

});

export default connect(mapStateToProps,{fetchItineraries})(Itineraris);