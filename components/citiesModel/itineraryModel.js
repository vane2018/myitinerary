/* const mongoose = require('mongoose');
const Schema= mongoose.Schema;

const itinerarySchema = new Schema({
 title:{
     type:String
 },
 profilePic:{
     type:String
 },
 rating:{
     type:Number
 },
 duration:{
     type:Number
 },
 price:{
     type: Number
 } */
/*  hashtap:{
     type:Array
 } */
//});

//module.exports = mongoose.model('itinerary', itinerarySchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const itinerarySchema=mongoose.Schema({
    profileImg: {
        type: String
      },
      profileName: {
        type: String
      },
      title: {
        type: String
      },
      likes: {
        type: Number
      },
      hours: {
        type: Number
      },
      price: {
        type: String
      },
      hashtags: {
        type: [String]
      },
     /*  city: {
        type: String
      }, */
   /*  hashtap:{
        type:Array
    } */

    cities: {
        type: String
      },
      activities: [{
        name: {
          type: String,
          required: [true, 'Name of activity is required']
        },
        about: {
          type: String
        },
        picture: {
          type: String,
          required: [true, 'Picture of activity is required']
        },
      }]
   });
    


module.exports = mongoose.model('iteneraris', itinerarySchema)