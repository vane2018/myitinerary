import React from 'react';
import { Link } from "react-router-dom";
import { Header } from './header'
import {Footer} from './footer'
import right from "../img/right.png";
import './link.css';
import './content.css';
import { BrowserRouter as Router} from "react-router-dom";



export default class Links extends React.Component {
    render() {
        
        return (
           <div>
                 {/* <Header /> */}
            <div className="content">
                
              <Router>

                 <a href="/cities"><img className="responsiveImg" src={right} alt="cities" /></a>
                 
</Router>
                <h4>Want to build your own MYtinerary?</h4>
                <ul>
                        <li><Link className="link" to="./login">Log In </Link></li>
                        <li><Link className="link" to="./signup">Create Account</Link></li>
                </ul>
            </div>
 {/*            <Footer />
 */}
           </div>
        
        )
    }
};