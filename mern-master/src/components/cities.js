import React from 'react';
import {getCities} from './action/citiesAction';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { Card, CardImg, CardTitle, CardImgOverlay } from "reactstrap";
import { NavLink } from "react-router-dom";

function searchingFor(term) {
  return function (x) {
    return x.name.toLowerCase().includes(term.toLowerCase()) || !term;
  };
}

class Cities extends React.Component {
 
   componentDidMount(){
    /*  const res =  await axios.get('http://localhost:5000/api/city');
     this.setState({listaPaises:res.data.cities});
     console.log(this.state.listaPaises); */
     this.props.getCities();
   }
  constructor() {
    super();
    this.state = {
      term: ""
    };
    this.search = this.search.bind(this);
  }
 

  search(event) {
    this.setState({ term: event.target.value });
  }


  /*   state ={
        listaPaises: []
    } */
 /*    static propType={
      getCities:this.propType.func.isRequired,
      city:PropTypes.object.isRequired
    }; */
  
    
     
    handleChange = (e) => {
      e.preventDefault();
      console.log(e.target.value);
      const { value } = e.target;
      this.setState({
          filter: value
      });
  }

    render() {
      console.log("ana")
      const cities = this.props.cities;
      console.log(cities); 
     /*  console.log(JSON.stringify(cities))*/
      return(

        <React.Fragment>
        <div className="input-group md-form form-lg form-1 pl-0">
          <div className="input-group-prepend">
            <span className="input-group-text cyan lighten-2" id="basic-text1">
              <i className="fas fa-search text-white" aria-hidden="true"></i>
            </span>
          </div>
          <input
            className="form-control my-0 py-1"
            type="text"
            onChange={this.search}
           
            placeholder="Search cities:"
            aria-label="Search"
          ></input>
        </div>
        <br></br>
        <div>
        
          {this.props.cities.filter(searchingFor(this.state.term)).map(cities => (
            <React.Fragment key={cities._id}>
              <NavLink to={`/itineraries/${cities.name}`}>
                <Card inverse className="cartas">
                  <CardImg src={cities.img} alt="Card image" className="cities" />
                  <CardImgOverlay>
                    <CardTitle className="card-title">{cities.name}</CardTitle>
                  </CardImgOverlay>
                </Card>
              </NavLink>
            </React.Fragment>
          ))}
        </div>
      </React.Fragment>


    /*   <div>

        <div>
            <label htmlFor="filter">Filter by Poet: </label>
            <input type="text" id="filter" 
            value={this.state.filter} 
             onChange={this.handleChange}/>
        </div>
         
        <ul>
        {cities.map((elem, i)=>{return <button><li key={i}>{elem.name}</li></button>})           }
        </ul>
     
     </div> */ )
 
   
        
    }
};

Cities.propTypes = {
  getCities: PropTypes.func.isRequired,
  cities: PropTypes.array.isRequired 
  
// isAuthenticated: PropTypes.bool
};
const mapStateToProps= state =>{
  console.log(state.city.cities);
  
  return{
  cities: state.city.cities}
};

export default  connect(mapStateToProps,{getCities})(Cities);