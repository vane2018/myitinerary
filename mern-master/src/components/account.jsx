import React from 'react';
import { connect } from 'react-redux';
import postUser from '../components/action/actionLogin'


const mapDispatchToProps = (dispatch) => {
    return {
        postUser: (data) => dispatch(postUser(data))
    }
};

class Account extends React.Component {

    constructor() {
        super()
        this.state = {
            username: "",
            password: "",
            email: "",
            profileImg:""
        }
    }

    onChange = (e) => {
        var state = this.state;
        state[e.target.name] = e.target.value;
        console.log(state);
        this.setState(state)
    }
    onSave = () => {
        this.props.postUser(this.state);
        this.setState({ username: "", password: "", email: "", profileImg: "" })
    }
    render() {
        return (
            <div>
                <h1>Create Account</h1>
                <form style={{ display: 'flex', flexDirection: 'column' }}>
                    <label htmlFor="">
                        userName:
                        <input value={this.state.username} type="text" name="userName" onChange={this.onChange} />
                    </label>
                    <label htmlFor="">
                        password:
                        <input value={this.state.password} type="text" name="password" onChange={this.onChange} />
                    </label>
                    <label htmlFor="">
                        email:
                        <input value={this.state.email} type="text" name="email" onChange={this.onChange} />
                    </label>
                    <label htmlFor="">
                        first name:
                        <input value={this.state.profileImg} type="text" name="profileImg" onChange={this.onChange} />
                    </label>
                  

                </form>
                <button onClick={this.onSave}>enviar</button>
            </div>
        )
    }
}
export default connect(null, mapDispatchToProps)(Account);